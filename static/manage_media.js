/* jshint browser: true */
/* jshint devel: true */
/* jshint esversion: 6 */
/* jshint globalstrict: true */

"use strict";

const checkboxes = document.querySelectorAll(".public-checkbox");

checkboxes.forEach(checkbox => {
	checkbox.addEventListener("change", function() {
		var url = checkbox.getAttribute("data-image-url");

		if (checkbox.checked) {
			url = `${url}/+public`;
		} else {
			url = `${url}/-public`;
		}

		fetch(url, {
			method: "PUT",
			cache: "no-store"
		})
		.then(res => {
			if (! res.ok)  {
				checkbox.checked = !checkbox.checked;
			}
		});
	});
});
