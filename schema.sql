-- sqlite

pragma journal_mode=WAL;

create table if not exists file_maps (
	album text not null,
	path text primary key,
	public bool not null,
	source text unique,
	url text unique,
	name text not null
);
