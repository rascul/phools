#!/bin/bash

rm woof.db
rm -r media/*
sqlite3 woof.db <schema.sql
cargo run --bin woofscan -- -l trace
