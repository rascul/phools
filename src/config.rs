//! Configuration
//!
//! First load configuration file, then replace anything with command line options
//!
//! Configuration file is checked for in the current directory, named `config.toml`,
//! or provided by the -c option

use std::default::Default;
use std::fs::File;
use std::io::{BufReader, Read};
use std::path::PathBuf;

use argh::FromArgs;
use serde::{Deserialize, Serialize};

use crate::{Dir, LogLevel, Result};

/// Configuration
#[derive(Clone, Debug, Deserialize, FromArgs, Serialize)]
pub struct Config {
	/// ip address to listen on (default: 127.0.0.1)
	#[argh(option, short = 'o', default = "default_address()")]
	#[serde(default = "default_address")]
	pub address: String,

	/// config file to use
	#[argh(option, short = 'c', default = "default_config_file()")]
	#[serde(default = "default_config_file")]
	pub config_file: PathBuf,

	/// path to sqlite db
	#[argh(option, short = 'b', default = "default_db()")]
	#[serde(default = "default_db")]
	pub db: Dir,

	/// directory for incoming files
	#[argh(option, short = 'i', default = "default_incoming()")]
	#[serde(default = "default_incoming")]
	pub incoming: Dir,

	/// album name for incoming files
	#[argh(option, short = 'a', default = "default_incoming_album()")]
	#[serde(default = "default_incoming_album")]
	pub incoming_album: Dir,

	/// log level
	#[argh(option, short = 'l', default = "default_level()")]
	#[serde(default = "default_level")]
	pub level: LogLevel,

	/// login name
	#[argh(option, default = "default_login_name()")]
	#[serde(default = "default_login_name")]
	pub login_name: String,

	/// login pass
	#[argh(option, default = "default_login_pass()")]
	#[serde(default = "default_login_pass")]
	pub login_pass: String,

	/// login salt
	#[argh(option, default = "default_login_salt()")]
	#[serde(default = "default_login_salt")]
	pub login_salt: String,

	/// path to media directory (default: media)
	#[argh(option, short = 'd', default = "default_media()")]
	#[serde(default = "default_media")]
	pub media: Dir,

	/// name of site
	#[argh(option, short = 'n', default = "default_name()")]
	#[serde(default = "default_name")]
	pub name: String,

	/// port to use (default: 8807)
	#[argh(option, short = 'p', default = "default_port()")]
	#[serde(default = "default_port")]
	pub port: u16,

	/// length of session before expires
	#[argh(option, short = 'e', default = "default_session_days()")]
	#[serde(default = "default_session_days")]
	pub session_days: u16,

	/// path to static file directory (default: static)
	#[argh(option, short = 's', default = "default_static_dir()")]
	#[serde(default = "default_static_dir")]
	pub static_dir: PathBuf,

	/// path to templates directory (default: templates)
	#[argh(option, short = 't', default = "default_templates()")]
	#[serde(default = "default_templates")]
	pub templates: PathBuf,

	/// url to access (default: http://localhost:8807)
	#[argh(option, short = 'u', default = "default_url()")]
	#[serde(default = "default_url")]
	pub url: String,

	/// number of http workers (default: 4)
	#[argh(option, short = 'w', default = "default_workers()")]
	#[serde(default = "default_workers")]
	pub workers: usize,
}

fn default_address() -> String {
	"127.0.0.1".into()
}

fn default_config_file() -> PathBuf {
	"config.toml".into()
}

fn default_db() -> Dir {
	"woof.db".into()
}

fn default_incoming() -> Dir {
	"incoming".into()
}

fn default_incoming_album() -> Dir {
	"incoming".into()
}

fn default_level() -> LogLevel {
	LogLevel::Info
}

fn default_login_name() -> String {
	"admin".into()
}

fn default_login_pass() -> String {
	"!".into()
}

fn default_login_salt() -> String {
	"pepper".into()
}
fn default_media() -> Dir {
	"media".into()
}

fn default_name() -> String {
	"medias".into()
}

fn default_port() -> u16 {
	8807
}

fn default_session_days() -> u16 {
	16
}

fn default_static_dir() -> PathBuf {
	"static".into()
}

fn default_templates() -> PathBuf {
	"templates".into()
}

fn default_url() -> String {
	"http://localhost:8807".into()
}

fn default_workers() -> usize {
	4
}

impl Default for Config {
	fn default() -> Self {
		Self {
			address: default_address(),
			config_file: default_config_file(),
			db: default_db(),
			incoming: default_incoming(),
			incoming_album: default_incoming_album(),
			level: default_level(),
			login_name: default_login_name(),
			login_pass: default_login_pass(),
			login_salt: default_login_salt(),
			media: default_media(),
			name: default_name(),
			port: default_port(),
			session_days: default_session_days(),
			static_dir: default_static_dir(),
			templates: default_templates(),
			url: default_url(),
			workers: default_workers(),
		}
	}
}

impl Config {
	/// load the config, from `config.toml` or what is passed to -c, or defaults, then
	/// overwrite with command line arguments
	pub fn load() -> Result<Self> {
		log::debug!("Loading arguments");
		let args = Self::load_args()?;
		log::debug!("args: {:#?}", args);

		log::debug!("Loading config file {} if it exists", args.config_file.display());
		let mut config = Self::load_file(&args.config_file).unwrap_or(Self::default());
		log::debug!("config: {:#?}", config);

		log::debug!("Loading config defaults");
		let default = Self::default();
		log::debug!("default: {:#?}", default);

		log::debug!("Merging config");
		if args.address != default.address && args.address != config.address {
			log::debug!("Merging address from args");
			config.address = args.address;
		}
		if args.config_file != default.config_file && args.config_file != config.config_file {
			log::debug!("Merging config_file from args");
			config.config_file = args.config_file;
		}
		if args.db != default.db && args.db != config.db {
			log::debug!("Merging db from args");
			config.db = args.db;
		}
		if args.incoming != default.incoming && args.incoming != config.incoming {
			log::debug!("Merging incoming from args");
			config.incoming = args.incoming;
		}
		if args.incoming_album != default.incoming_album
			&& args.incoming_album != config.incoming_album
		{
			log::debug!("Merging incoming_album from args");
			config.incoming_album = args.incoming_album;
		}
		if args.level != default.level && args.level != config.level {
			log::debug!("Merging level from args");
			config.level = args.level;
		}
		if args.login_name != default.login_name && args.login_name != config.login_name {
			log::debug!("Merging login_name from args");
			config.login_name = args.login_name;
		}
		if args.login_pass != default.login_pass && args.login_pass != config.login_pass {
			log::debug!("Merging login_pass from args");
			config.login_pass = args.login_pass;
		}
		if args.login_salt != default.login_salt && args.login_salt != config.login_salt {
			log::debug!("Merging login_salt from args");
			config.login_salt = args.login_salt;
		}
		if args.media != default.media && args.media != config.media {
			log::debug!("Merging media from args");
			config.media = args.media;
		}
		if args.name != default.name && args.name != config.name {
			log::debug!("Merging name from args");
			config.name = args.name;
		}
		if args.port != default.port && args.port != config.port {
			log::debug!("Merging port from args");
			config.port = args.port;
		}
		if args.session_days != default.session_days && args.session_days != config.session_days {
			log::debug!("Merging session_days from args");
			config.session_days = args.session_days;
		}
		if args.static_dir != default.static_dir && args.static_dir != config.static_dir {
			log::debug!("Merging static_dir from args");
			config.static_dir = args.static_dir;
		}
		if args.templates != default.templates && args.templates != config.templates {
			log::debug!("Merging templates from args");
			config.templates = args.templates;
		}
		if args.url != default.url && args.url != config.url {
			log::debug!("Merging url from args");
			config.url = args.url;
		}
		if args.workers != default.workers && args.workers != config.workers {
			log::debug!("Merging workers from args");
			config.workers = args.workers;
		}

		Ok(config)
	}

	/// load config file
	fn load_file(p: &PathBuf) -> Result<Self> {
		let file = File::open(p)?;
		let mut buf = BufReader::new(file);
		let mut contents = String::new();

		buf.read_to_string(&mut contents)?;

		Ok(toml::from_str(&contents)?)
	}

	/// load args from command line
	fn load_args() -> Result<Self> {
		Ok(argh::from_env())
	}
}
