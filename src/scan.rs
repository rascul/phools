mod config;
mod dir;
mod log_level;
mod logger;
mod result;

use std::fs::{create_dir_all, hard_link};

use log::{debug, error, info};
use r2d2_sqlite::rusqlite::Connection;

use config::Config;
use dir::Dir;
use log_level::LogLevel;
use result::Result;

fn main() -> Result<()> {
	logger::init()?;

	let config = Config::load()?;
	logger::set_level(config.level.clone());

	let db = Connection::open(&config.db)?;

	debug!("Config: {:#?}", config);

	info!("Scanning {}", config.incoming);
	scan_dir(&db, &config, &config.incoming, &Dir::new())?;

	Ok(())
}

fn scan_dir(db: &Connection, config: &Config, dir: &Dir, orig_url: &Dir) -> Result<()> {
	debug!("Entering directory: {}", dir);

	debug!("orig_url: {orig_url}");

	for entry in dir.read_dir()? {
		debug!("Entry: {entry:?}");
		let entry = match entry {
			Ok(entry) => entry,
			Err(e) => {
				error!("Bad directory entry: {}: {:?}", e, e);
				continue;
			}
		};

		let source: Dir = entry.path().into();

		info!("Source: {source}");

		let file_name: Dir = if let Some(p) = entry.path().file_name() {
			p.into()
		} else {
			error!("Bad file name: {:?}", entry);
			continue;
		};

		debug!("file_name: {file_name}");

		let path = config.media.join(config.incoming_album.join(&orig_url.join(&file_name)));

		debug!("Path: {path}");

		let url: Dir = Dir::from("/").join(config.incoming_album.join(&orig_url.join(&file_name)));

		debug!("Url: {url}");

		if entry.path().is_dir() {
			scan_dir(&db, &config, &entry.path().into(), &orig_url.join(&entry.file_name()))?;
			continue;
		}

		debug!("Add to DB");

		let query = "
			insert into file_maps (
				'album', 'path', 'public', 'source', 'url', 'name'
			) values (
				?1, ?2, ?3, ?4, ?5, ?6
			)";

		let album = if !orig_url.to_string().to_string().is_empty() {
			config.incoming_album.join(&orig_url)
		} else {
			config.incoming_album.clone()
		};

		debug!("Album: {album}");

		if let Err(e) = db.execute(query, (&album, &path, false, &source, &url, &file_name)) {
			error!("SQL Error: {}", e);
			continue;
		} else {
			info!("Added {url}");
		}

		let parent = path.parent();
		debug!("Parent: {:?}", &parent);

		if !parent.is_dir() {
			match create_dir_all(&parent) {
				Ok(_) => info!("Created directory: {}", &path),
				Err(e) => match e.kind() {
					std::io::ErrorKind::AlreadyExists => {
						debug!("Directory exists: {}", &parent.clone())
					}
					_ => {
						error!("Error creating directory {}: {}", &path, e);
						continue;
					}
				},
			}
		};

		debug!("Hardlink: {path}");

		if let Err(e) = hard_link(entry.path(), &path.path()) {
			error!("Hard link error: {}", e);
			continue;
		} else {
			info!("Hard linked {source} to {path}");
		}
	}

	Ok(())
}
