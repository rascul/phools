mod config;
//mod db;
mod dir;
mod log_level;
mod logger;
#[macro_use]
mod macros;
mod result;
mod routes;

use std::collections::HashMap;
use std::sync::Mutex;

use actix_files::Files;
use actix_web::cookie::time::OffsetDateTime;
use actix_web::web::{to, Data};
use actix_web::{middleware, App, HttpServer};

use log::{debug, info};
use tera::Tera;

use config::Config;
use dir::Dir;
use log_level::LogLevel;

use r2d2::Pool;
use r2d2_sqlite::SqliteConnectionManager;

use result::Result;

pub type Db = Pool<SqliteConnectionManager>;
pub type Sessions = HashMap<String, OffsetDateTime>;

fn main() -> Result<()> {
	logger::init()?;

	let config = Config::load()?;
	logger::set_level(config.level.clone());

	debug!("Config: {:#?}", config);

	serve(&config)?;

	Ok(())
}

/// entry point for the web app
#[actix_web::main]
pub async fn serve(config: &Config) -> std::io::Result<()> {
	let clone_config = config.clone();

	info!("Starting http server for {} at {}:{}", &config.url, &config.address, &config.port);

	let sessions: Data<Mutex<Sessions>> = Data::new(Mutex::new(Sessions::new()));
	let db = Db::new(SqliteConnectionManager::file(&clone_config.db)).unwrap();

	HttpServer::new(move || {
		let tdir = clone_config.templates.to_str().unwrap();
		let tmpl = Tera::new(&format!("{tdir}/*")).unwrap();

		App::new()
			.app_data(Data::new(tmpl))
			.app_data(Data::new(db.clone()))
			.app_data(sessions.clone())
			.app_data(Data::new(clone_config.clone()))
			.wrap(middleware::Logger::default())
			.service(routes::login::get)
			.service(routes::login::post)
			.service(routes::public::put)
			.service(Files::new("/static", "static").show_files_listing())
			.default_service(to(routes::default::get))
	})
	.bind((config.address.clone(), config.port))?
	.workers(config.workers)
	.run()
	.await
}
