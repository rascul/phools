/// Logger implementation for colors in the terminal and in the future
/// logging to a file and to a remote log server
use colored::*;
use log::{set_logger, set_max_level, Level, LevelFilter, Log, Metadata, Record, SetLoggerError};

use crate::LogLevel;

static LOGGER: Logger = Logger;

pub struct Logger;

impl Log for Logger {
	fn enabled(&self, metadata: &Metadata) -> bool {
		metadata.level() <= Level::Trace
	}

	fn log(&self, record: &Record) {
		if self.enabled(record.metadata()) {
			let (msg, _level) = match record.level() {
				Level::Error => (format!("{} {}", "ERR".red(), record.args()), 3),
				Level::Warn => (format!("{} {}", "WRN".purple(), record.args()), 4),
				Level::Info => (format!("{} {}", "INF".cyan(), record.args()), 6),
				Level::Debug => (format!("{} {}", "DBG".yellow(), record.args()), 7),
				Level::Trace => (format!("{} {}", "TRC".green(), record.args()), 0),
			};
			println!("{}", msg);
		}
	}

	fn flush(&self) {}
}

pub fn init() -> Result<(), SetLoggerError> {
	Ok(set_logger(&LOGGER).map(|()| set_max_level(LevelFilter::Info))?)
}

pub fn set_level(level: LogLevel) {
	set_max_level(level.into());
}
