use std::sync::Mutex;

use actix_web::cookie::time::{Duration, OffsetDateTime};
use actix_web::cookie::Cookie;
use actix_web::web::{Data, Form, Redirect};
use actix_web::{error, get, post, HttpRequest, HttpResponse, Responder, Result};

use chrono::Utc;
use log::{error, info};
use pretty_sha2::sha512;
use serde::{Deserialize, Serialize};
use tera::Tera;

use crate::{Config, Sessions};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct FormData {
	name: String,
	password: String,
}

/// login form
#[get("/login")]
pub async fn get(tmpl: Data<Tera>, config: Data<Config>) -> Result<impl Responder> {
	let mut context = tera::Context::new();
	context.insert("name", &config.name);
	context.insert("url", &config.url);

	return Ok(HttpResponse::Ok().body(
		tmpl.render("login.html", &context).map_err(|e| error::ErrorInternalServerError(e))?,
	));
}

/// do the actual login
#[post("/login")]
pub async fn post(
	sessions: Data<Mutex<Sessions>>,
	form_data: Form<FormData>,
	req: HttpRequest,
	config: Data<Config>,
) -> Result<impl Responder> {
	// make sure login info is correct
	if form_data.name != config.login_name || form_data.password != config.login_pass {
		info!(
			"Login attempt failed from {:?} user {} pass {}",
			req.peer_addr(),
			form_data.name,
			form_data.password
		);

		return Ok(Redirect::to("/login").see_other().respond_to(&req));
	}

	// create new session id which is sha512 hash of client ip + timestamp + some random data
	let hash = sha512::gen(
		[
			match req.peer_addr() {
				Some(p) => p.to_string(),
				None => "0.0.0.0".into(),
			},
			Utc::now().to_rfc3339(),
			rand::random::<[char; 30]>().iter().collect(),
		]
		.concat(),
	);

	// add session to sessions listing
	match sessions.lock() {
		Ok(mut sessions) => {
			let expires = OffsetDateTime::now_utc() + Duration::days(90);
			sessions.insert(hash.clone(), expires);
		}
		Err(e) => {
			error!("Unable to get sessions lock: {e}");
		}
	}

	info!("Successful login from {:?} user {}", req.peer_addr(), form_data.name);

	let mut res = Redirect::to("/").see_other().respond_to(&req);
	let mut cookie = Cookie::new(&config.name, &hash);
	cookie.set_http_only(true);
	cookie.set_path("/");
	cookie.set_max_age(Duration::days(90));
	res.add_cookie(&cookie).map_err(|e| error::ErrorInternalServerError(e))?;

	Ok(res)
}
