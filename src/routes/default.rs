use std::sync::Mutex;

use actix_files::NamedFile;
use actix_web::cookie::time::{Duration, OffsetDateTime};
use actix_web::cookie::Cookie;
use actix_web::web::{block, Data};
use actix_web::{error, HttpRequest, HttpResponse, Responder, Result};

use log::{debug, error};

use serde::{Deserialize, Serialize};
use serde_rusqlite::{from_row, from_rows};

use tera::Tera;

use crate::{Config, Db, Dir, Sessions};

#[derive(Clone, Debug, Serialize)]
pub struct Album {
	name: String,
	url: Dir,
}

pub async fn get(
	req: HttpRequest,
	tmpl: Data<Tera>,
	db: Data<Db>,
	sessions: Data<Mutex<Sessions>>,
	config: Data<Config>,
) -> Result<impl Responder> {
	// get the url path
	let url: Dir = req.path().into();

	// get rid of the leading / to make it relative
	let album: Dir =
		if req.path().is_empty() { "" } else { &req.path().trim_start_matches('/') }.into();

	// path to album including media dir
	let path: Dir = config.media.join(&album).into();

	// make sure the path exists
	if !path.exists() {
		return Ok(HttpResponse::NotFound().finish());
	}

	// start the context to pass to the template
	let mut context = tera::Context::new();
	context.insert("name", &config.name);
	context.insert("url", &config.url);

	// figure out if we're logged in and what the session id is
	let (logged_in, session_id): (bool, String) = if let Some(cookie) = req.cookie(&config.name) {
		let cookie_session = cookie.value();

		// get a lock on the sessions mutex or throw a fit
		match sessions.lock() {
			Ok(sessions) => {
				let sessions = sessions.clone();

				if let Some(expires) = sessions.get(cookie_session) {
					// check that the session isn't yet expired
					if expires > &OffsetDateTime::now_utc() {
						context.insert("id", &config.login_name);
						(true, cookie_session.to_string())
					} else {
						(false, String::new())
					}
				} else {
					(false, String::new())
				}
			}
			Err(e) => {
				error!("{} ({}): Unable to get lock on sessions: {e}", file!(), line!());
				(false, String::new())
			}
		}
	} else {
		(false, String::new())
	};

	// if it's a directory then it's an album and we need to list things
	if path.is_dir() {
		// see if there's albums (subdirectories) to list
		if let Ok(albums_list) = &path.albums() {
			let mut albums = Vec::new();

			for a in albums_list {
				let name = file_name_or_continue!(a).to_string();
				let url = url.join(&name);
				albums.push(Album { name, url });
			}

			context.insert("albums", &albums);
		}

		debug!("searching db for album {album}");

		let conn =
			block(move || db.get()).await?.map_err(|e| error::ErrorInternalServerError(e))?;
		let mut statement = if logged_in {
			conn.prepare("select url, public from file_maps where album = ?1").unwrap()
		} else {
			conn.prepare("select url, public from file_maps where album = ?1 and public = true")
				.unwrap()
		};

		// i think this one needs to be blocked and awaited?
		let media_list: Vec<DbMedia> =
			from_rows::<DbMedia>(statement.query([&album]).unwrap()).map(|r| r.unwrap()).collect();

		let mut medias = Vec::new();
		for m in media_list {
			medias.push(m);
		}
		if !medias.is_empty() {
			context.insert("medias", &medias);
		}

		// get the url path components for the path links
		let url_path_components: Vec<String> = url
			.components()
			.filter_map(|c| {
				if let Some(s) = c.as_os_str().to_str() {
					if s == "/" {
						None
					} else {
						Some(s.to_string())
					}
				} else {
					None
				}
			})
			.collect();
		context.insert("path", &url_path_components);

		// build http response from template and context
		let mut res = HttpResponse::Ok().body(
			tmpl.render("index.html", &context).map_err(|e| error::ErrorInternalServerError(e))?,
		);

		// maybe add session cookie
		if logged_in {
			let mut cookie = Cookie::new(&config.name, session_id);
			cookie.set_http_only(true);
			cookie.set_path("/");
			cookie.set_max_age(Duration::days(90));
			res.add_cookie(&cookie).unwrap();
		}

		return Ok(HttpResponse::Ok().body(
			tmpl.render("index.html", &context).map_err(|e| error::ErrorInternalServerError(e))?,
		));
	}

	if path.is_file() {
		let conn = db.get().map_err(|e| error::ErrorInternalServerError(e))?;
		let mut statement =
			conn.prepare("select * from file_maps where path = ?1")
			.map_err(|e| error::ErrorInternalServerError(e))?;
		//let media_list: DbMedia =
		//	from_row::<DbMedia>(statement.query_row([&album]).unwrap()).map(|r| r.unwrap()).collect();
		let db_media: DbMedia = {
			statement.query_row().unwrap()
		};
	}


	Ok(NamedFile::open_async(&path.to_string()).await?.into_response(&req))
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct DbMedia {
	url: String,
	public: bool,
}
