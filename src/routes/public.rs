use std::sync::Mutex;

use actix_web::cookie::time::OffsetDateTime;
use actix_web::web::{block, Data};
use actix_web::{error, put, HttpRequest, HttpResponse, Responder, Result};

use log::error;

use crate::{Config, Db, Sessions};

/// login form
#[put("{image:.*}/{op}public")]
pub async fn put(
	req: HttpRequest,
	config: Data<Config>,
	db: Data<Db>,
	sessions: Data<Mutex<Sessions>>,
) -> Result<impl Responder> {
	// figure out if we're logged in and what the session id is
	if let Some(cookie) = req.cookie(&config.name) {
		let cookie_session = cookie.value();

		// get a lock on the sessions mutex or throw a fit
		match sessions.lock() {
			Ok(sessions) => {
				let sessions = sessions.clone();

				if let Some(expires) = sessions.get(cookie_session) {
					// check that the session isn't yet expired
					if expires < &OffsetDateTime::now_utc() {
						return Err(error::ErrorUnauthorized("unauthorized"));
					}
				} else {
					return Err(error::ErrorUnauthorized("unauthorized"));
				}
			}
			Err(e) => {
				error!("{} ({}): Unable to get lock on sessions: {e}", file!(), line!());
				return Err(error::ErrorInternalServerError(e.to_string()));
			}
		}
	} else {
		return Err(error::ErrorUnauthorized("unauthorized"));
	};

	// extract image path from url
	let image = match req.match_info().get("image") {
		Some(image) => ["/", &image.to_string()].concat(),
		None => return Err(error::ErrorBadRequest("image not specified")),
	};

	// extract operator from url to determine public boolean
	let public = match req.match_info().get("op") {
		Some("+") => true,
		Some("-") => false,
		_ => return Err(error::ErrorBadRequest("bad op")),
	};

	let conn = block(move || db.get()).await?.map_err(|e| error::ErrorInternalServerError(e))?;

	match block(move || {
		conn.execute("update file_maps set public = ?1 where url = ?2", (public, image))
	})
	.await?
	.map_err(|e| error::ErrorInternalServerError(e))
	{
		Ok(1) => Ok(HttpResponse::Ok()),
		Ok(r) => Err(error::ErrorInternalServerError(format!("bad number of rows: {r}"))),
		Err(e) => Err(error::ErrorInternalServerError(e)),
	}
}
