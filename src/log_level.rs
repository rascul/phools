use std::convert::Into;
use std::str::FromStr;

use log::{Level, LevelFilter};
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub enum LogLevel {
	Error,
	Warn,
	Info,
	Debug,
	Trace,
}

// this one is for arg processing with argh
impl FromStr for LogLevel {
	type Err = String;

	fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
		Ok(match s {
			"error" => Self::Error,
			"warn" => Self::Warn,
			"info" => Self::Info,
			"debug" => Self::Debug,
			"trace" => Self::Trace,
			_ => Self::Info,
		})
	}
}

impl Into<Level> for LogLevel {
	fn into(self) -> Level {
		match self {
			Self::Error => Level::Error,
			Self::Warn => Level::Warn,
			Self::Info => Level::Info,
			Self::Debug => Level::Debug,
			Self::Trace => Level::Trace,
		}
	}
}

impl Into<LevelFilter> for LogLevel {
	fn into(self) -> LevelFilter {
		match self {
			Self::Error => LevelFilter::Error,
			Self::Warn => LevelFilter::Warn,
			Self::Info => LevelFilter::Info,
			Self::Debug => LevelFilter::Debug,
			Self::Trace => LevelFilter::Trace,
		}
	}
}
