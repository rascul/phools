use std::convert::AsRef;
use std::ffi::OsStr;
use std::fmt;
use std::ops::Deref;
use std::path::{Path, PathBuf};
use std::str::FromStr;

use r2d2_sqlite::rusqlite::types::{FromSql, FromSqlResult, ToSqlOutput, ValueRef};
use r2d2_sqlite::rusqlite::{Result as SqlResult, ToSql};

use serde::{Deserialize, Serialize};

use crate::Result;

#[derive(Clone, Deserialize, PartialEq, Serialize)]
pub struct Dir(PathBuf);

impl Deref for Dir {
	type Target = PathBuf;

	fn deref(&self) -> &Self::Target {
		&self.0
	}
}

// to print the name for debug
impl fmt::Debug for Dir {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "{}", self.0.display())
	}
}

// to print the name
impl fmt::Display for Dir {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "{}", self.0.display())
	}
}

// this one is for arg processing with argh
impl FromStr for Dir {
	type Err = String;

	fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
		Ok(Self(s.into()))
	}
}

// so we can do: `let m = "media".into()` and get a `Dir`
impl From<&str> for Dir {
	fn from(s: &str) -> Self {
		Self(s.into())
	}
}

impl From<&OsStr> for Dir {
	fn from(s: &OsStr) -> Self {
		Self(s.into())
	}
}

impl From<PathBuf> for Dir {
	fn from(p: PathBuf) -> Self {
		Self(p)
	}
}

impl AsRef<Path> for Dir {
	fn as_ref(&self) -> &Path {
		&self.0
	}
}

impl ToSql for Dir {
	fn to_sql(&self) -> SqlResult<ToSqlOutput> {
		Ok(self.to_string().into())
	}
}

impl FromSql for Dir {
	fn column_result(value: ValueRef<'_>) -> FromSqlResult<Self> {
		Ok(value.as_str()?.into())
	}
}

impl Dir {
	/// list all albums (directories) in the directory
	#[allow(dead_code)]
	pub fn albums(self: &Self) -> Result<Vec<Dir>> {
		let mut albums: Vec<Dir> = Vec::new();

		for entry in self.0.read_dir()? {
			let p = entry?.path();
			if p.is_dir() {
				albums.push(p.into());
			}
		}

		Ok(albums)
	}

	/// return a `PathBuf`
	pub fn path(&self) -> PathBuf {
		self.0.clone()
	}

	/// join
	pub fn join<P: AsRef<Path>>(&self, path: P) -> Self {
		Self(self.0.join(path))
	}

	/// new
	pub fn new() -> Self {
		Self(PathBuf::new())
	}

	pub fn parent(&self) -> Dir {
		if let Some(p) = self.0.parent() {
			p.to_owned().into()
		} else {
			".".into()
		}
	}
}
