#[macro_export]
macro_rules! serr {
	($($arg:tt)*) => {
		Err(Box::from([$($arg)*].concat()))
	};
}

#[macro_export]
macro_rules! file_name_or_continue {
	($name:ident) => {
		if let Some(name) = $name.file_name() {
			if let Some(n) = name.to_str() {
				n
			} else {
				error!("{} ({}): Bad file name: {name:?}", file!(), line!());
				continue;
			}
		} else {
			error!("{} ({}): Bad file name: {}", file!(), line!(), $name);
			continue;
		}
	};
}

// let name = if let Some(name) = m.file_name() {
// 	if let Some(n) = name.to_str() {
// 		n
// 	} else {
// 		error!("{} ({}): Bad file name: {name:?}", file!(), line!());
// 		continue;
// 	}
// } else {
// 	error!("{} ({}): Bad file name: {m}", file!(), line!());
// 	continue;
// };
